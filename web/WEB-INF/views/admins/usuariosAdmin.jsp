
<%@page import="java.util.List"%>
<%@page import="Entity.Acceso"%>
<%@page import="Helpers.AppHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="imgs/42697fire_98982.ico" />
        <!-- Core Stylesheet -->
        <link href="style.css" rel="stylesheet">

        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
        <title>INCOSBO</title>
    </head>
</head>
<body onload="denegado();">
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="mosh-preloader"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area clearfix">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <!-- Menu Area Start -->
                <div class="col-12 h-100">
                    <div class="menu_area h-100">
                        <nav class="navbar h-100 navbar-expand-lg align-items-center">
                            <!-- Logo -->
                            <a class="navbar-brand" href="<%=AppHelper.baseUrl()%>adminProductos"><img src="imgs/Logo_Incosbo_02.png" width="180" height="100" alt="logo"></a>

                            <!-- Menu Area -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                            <div class="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                                <ul class="navbar-nav animated" id="nav">
                                    <a class="nav-link" href="<%=AppHelper.baseUrl()%>adminProductos">Inicio</a>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminProductos">Productos</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminServicios">Servicios</a></li>
                                    <%HttpSession sesiones;
                                    sesiones = request.getSession(false);
                                    if(AppHelper.tieneAcceso(sesiones, 1)){%>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminUsuarios">Usuarios</a></li>
                                    <%}%>
                                </ul>
                                <!-- Login-->
                                <div class="login-register-btn">
                                    <a onclick=""  href="<%=AppHelper.baseUrl()%>logout"
                                       data-toggle="" data-target="">Cerrar sesión</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    <!-- 
    Rojo-- contra incendio
    Azul--- hidrosanitarios
    -->

    <section class="mosh-workflow-area section_padding_100_0 clearfix" style="background-image: url(imgs/welcome-bg.png)">
        <div class="container">

            <div class="row"> 
                <div class="col-sm-12" align="center">
                    <button class="btn btn-group" align="center" onClick="#myModal" href="" data-toggle="modal" data-target="#addMod">Agregar nuevo usuario</button> 
                </div>
                &nbsp;
                <div class="col-sm-12" align="center">
                    <%if (session.getAttribute("mensajeOk") != null) { %>
                    <div class="col-sm-12">
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>¡Correcto!</strong> El usuario fue registrado
                        </div>
                    </div>
                    <% session.removeAttribute("mensajeOk");
                        }
                    %>
                    <%if (session.getAttribute("mensajeError") != null) { %>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>¡Error!</strong> El usuario ya está registrado
                    </div>
                    <% session.removeAttribute("mensajeError");
                        }
                    %>
                </div>
                <div class="table-responsive">
                    <% List<Acceso> usuarios = (List<Acceso>) request.getAttribute("busqueda");%>

                    <table class="table table-bordered table-dark"> 
                        <%if (usuarios != null) {
                                for (Acceso acceso : usuarios) {
                                    System.out.println("Email: " + acceso.getEmail());
                                    System.out.println("password " + acceso.getPassword());
                                    System.out.println("usuario " + acceso.getTipUsu());%>
                        <tr>
                            <th>Correo</th>
                            <td>Tipo de usuario</td>
                            <td>Acciones</td>
                        </tr>
                        <tr>

                            <td style="text-align: center"><%=acceso.getEmail()%></td>       
                            <%if (acceso.getTipUsu() == 1) {%>
                            <td style="text-align: center">Super Administrador</td>
                            <%} else {%>
                            <td style="text-align: center">Administrador</td>
                            <%}%>
                            <td style="text-align: center">
                                <button type="button" 
                                        class="btn btn-info"
                                        data-id="<%=acceso.getIdUsu()%>"
                                        data-email="<%=acceso.getEmail()%>"
                                        data-password="<%=acceso.getPassword()%>"
                                        data-usu="<%=acceso.getTipUsu()%>"
                                        onClick="#myModal" 
                                        href="" 
                                        data-toggle="modal" 
                                        data-target="#editMod">
                                    Editar
                                </button>
                                <button type="button" 
                                        id="eliminar"
                                        data-id="<%=acceso.getIdUsu()%>"
                                        class="mode btn-danger btn">
                                    Eliminar
                                </button>
                            </td>
                            <%}
                            } else {%>
                            <td>No existen registros</td>                                
                            <%}%>
                        </tr>
                    </table>
                </div>
        &nbsp;
        &nbsp;

        <!--Modal add-->
        <form action="addUsu" method="POST">
            <div class="modal fade" id="addMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Agregar nuevo usuario</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="email" class="form-control" id="emailModal" name="emailModal" required minlength="8"  maxlength="45">

                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" class="form-control" id="passModal" name="passModal" required maxlength="45" minlength="6">
                                <label for="tipUsu" class="control-label">Nivel de Usuario</label>
                                <select  class="form-control" id="usuario" name="usuario">
                                    <option value="1">Super Administrador</option>
                                    <option value="2">Administrador</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="agregar">Agregar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--End Modal add-->

        <!--Modal edit-->
        <form action="editUsu"  method="POST">
            <div class="modal fade" id="editMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Actualizar usuario</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>    
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="idEdit" name="idEdit" required maxlength="45">
                                <label for="email" class="control-label">Email</label>
                                <input type="email" class="form-control" id="emailEdit" name="emailEdit" required minlength="8"  maxlength="45">

                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" class="form-control" id="passEdit" name="passEdit" required maxlength="45">
                                <label for="tipUsu" class="control-label">Nivel de Usuario</label>
                                <select  class="form-control" id="usuEdit" name="usuEdit">
                                    <option value="1">Super Administrador</option>
                                    <option value="2">Administrador</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-id="log">Editar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--End Modal edit-->
</div>
        </div>   
    </section>

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area clearfix">
        <!-- Top Fotter Area -->
        <footer class="footer-area clearfix">
            <!-- Top Fotter Area -->
            <div class="top-footer-area section_padding_100_0">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-footer-widget mb-100">
                                <a href="#" class="mb-50 d-block"><img src="imgs/Logo_Incosbo_02.png" alt=""></a>

                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-footer-widget mb-100">
                                <p>Misión</p>
                                <p>Ser una empresa rentable buscando un crecimiento estable en la comercialización de nuestros productos y servicios, siendo la mejor calificada y reconocida en México por calidad y servicio de nuestro equipo.</p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-footer-widget mb-100">
                                <p>Visión</p>
                                <p>Consolidarse como una empresa competitiva a nivel nacional, siendo la primera alternativa para nuestros clientes, con el desarrollo de nuestros equipos, manteniendo una filosofía de mejora continua que nos permita destacar en el sector industrial y del agua</p>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-footer-widget mb-100">
                                <div class="footer-single-contact-info d-flex">
                                    <div class="contact-icon">
                                        <img src="imgs/map.png" alt="">
                                    </div>
                                    <p>Calle Tomillo 16 Manzana 604, Colonia El Romerillal, Querétaro Qro C.P. 76118</p>
                                </div>
                                <div class="footer-single-contact-info d-flex">
                                    <div class="contact-icon">
                                        <img src="imgs/call.png" alt="">
                                    </div>
                                    <p>(442) 5377021 <br>(442) 7130033</p>
                                </div>
                                <div class="footer-single-contact-info d-flex">
                                    <div class="contact-icon">
                                        <img src="imgs/message.png" alt="">
                                    </div>
                                    <p>ventas@incosbo.com.mx</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fotter Bottom Area -->
            <div class="footer-bottom-area">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 h-100">
                            <div class="footer-bottom-content h-100 d-md-flex justify-content-between align-items-center">
                                <div class="copyright-text">
                                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
                                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    </p>
                                </div>
                                <div class="footer-social-info">
                                    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- ***** Footer Area End ***** -->
<%request.removeAttribute("busqueda");%>
</body>
<!-- jQuery-2.2.4 js -->
<script src="js/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/popper.min.js"></script> 
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>       
<!-- All Plugins js -->
<script src="js/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>
<script>
        function denegado(){
            window.location.hash="no-back-button";
            window.location.hash="Again-No-back-button" //chrome
            window.onhashchange=function(){window.location.hash="no-back-button";}
        }
</script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.mode').click(function () {
            var id = $(this).attr("data-id");
            var conf = confirm("¿Estas seguro que quieres eliminar a este usuario?");
            if (conf) {
                $.ajax({
                    url: "deliUsu",
                    type: "POST",
                    data: {
                        usuario: $(this).attr("data-id")
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function () {}
                    });
            }
        });
    });
</script>

<script>

    $('#editMod').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var email = button.data('email');
        var usu = button.data('usu');

        var modal = $(this);
        modal.find('.modal-body #emailEdit').val(email);
        modal.find('.modal-body #idEdit').val(id);
        modal.find('.modal-body #usuEdit').val(usu);
    });
</script>
</html>