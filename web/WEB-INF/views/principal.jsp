<%@page import="Helpers.AppHelper"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        
        <meta name="google-site-verification" content="hQqcfoVcg4_A5I-lmgabvhHTWCYdLPzR9-GFVdfuGI4" />
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="imgs/42697fire_98982.ico" />
        <!-- Core Stylesheet -->
        <link href="style.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
        <title>INCOSBO</title>
    </head>
    <body>
        
        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="mosh-preloader"></div>
        </div>

        <!-- ***** Header Area Start ***** -->
        <header class="header_area clearfix">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <!-- Menu Area Start -->
                    <div class="col-12 h-100">
                        <div class="menu_area h-100">
                            <nav class="navbar h-100 navbar-expand-lg align-items-center">
                                <!-- Logo -->
                                <a class="navbar-brand" href="<%=AppHelper.baseUrl()%>inicio"><img src="imgs/Logo_Incosbo_12.png" width="180" height="100" alt="logo"></a>

                                <!-- Menu Area -->
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                                <div class="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                                    <ul class="navbar-nav animated" id="nav">                                        
                                        <a class="nav-link" href="<%=AppHelper.baseUrl()%>inicio">Inicio</a>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>conocenos">Con�cenos</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>productos">Productos</a></li>
                                        <li class="nav-item"><a class="nav-link" href=<%=AppHelper.baseUrl()%>servicios>Servicios</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>contacto">Contacto</a></li>
                                    </ul>
                                    <!-- Login-->
                                    <div class="login-register-btn">
                                        <a onclick="#myModal" href=""
                                           data-toggle="modal" data-target="#loginMod">Iniciar sesi�n</a>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- ***** Header Area End ***** -->
        <!-- ***** Welcome Area Start ***** -->
        <section class="welcome_area clearfix" id="home" style="background-image: url(imgs/welcome-bg.png)">
            <div class="hero-slides owl-carousel">
                <!-- Single Hero Slides -->
                <div class="single-hero-slide d-flex align-items-end justify-content-center">
                    <div class="hero-slide-content text-center" >
                        <h2>Paquete integral Contra-Incendio</h2>
                        <a href="<%=AppHelper.baseUrl()%>productos"><img class="slide-img" style="height: 500px; width: 750px;" src="imgs/corte30.png" alt=""></a>
                    </div>
                </div>
                <!-- Single Hero Slides -->
                <div class="single-hero-slide d-flex align-items-end justify-content-center">
                    <div class="hero-slide-content text-center">
                        <h2  style="font-size: 28px;">Motor de combusti�n interna y bomba horizontal</h2>
                        <h2  style="font-size: 28px;">para sistemas Contra-Incendio</h2>
                        <a href="<%=AppHelper.baseUrl()%>productos"><img class="slide-img" style="height: 500px; width: 750px;" src="imgs/diesel 500.png" alt=""></a>
                    </div>
                </div>
                <!-- Single Hero Slides -->
                <div class="single-hero-slide d-flex align-items-end justify-content-center">
                    <div class="hero-slide-content text-center">
                        <h2>Tablero controlador de bombas</h2>
                        <a href="<%=AppHelper.baseUrl()%>productos"><img class="slide-img" style="height: 500px; width: 750px;" src="imgs/DSC_0173.png" alt=""></a>
                    </div>
                </div>
                <!-- Single Hero Slides -->
                <div class="single-hero-slide d-flex align-items-end justify-content-center">
                    <div class="hero-slide-content text-center">
                        <h2 style="font-size: 35px;">Paquete de bombeo hidroneum�tico de presi�n variable</h2>
                        <a href="<%=AppHelper.baseUrl()%>productos"><img class="slide-img" style="height: 500px; width: 750px;" src="imgs/20151204_142404.png" alt=""></a>
                    </div>
                </div>
            </div>        
            <!-- ***** Service Area Start ***** -->
        </section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <h1>Nuestras categorias</h1>
                    </center>
                </div>
            </div>
        </div>            
        <section class="mosh-more-services-area d-sm-flex clearfix">
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay=".4s">
                    <a id="h1"><img src="imgs/Hidrosanitaria.png" width="100" height="200"></a>
                    <h4>Hidrosanitarias</h4>
                </div>
            </div>
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay=".7s">
                    <a id="h2"><img src="imgs/Aguas Pluviales.png" width="145" height="300"></a>
                    <h4>Aguas Pluviales</h4>
                </div>
            </div>
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay="1s">
                    <a id="h3"><img src="imgs/Contra-Incendio.png" width="87" height="100"></a>
                    <h4>Contra-incendio</h4>
                </div>
            </div>
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay=".1s">
                    <a id="h4"><img src="imgs/Pozos y redes de agua municipales.png" width="70" height="100"></a>
                    <h4>Pozos y redes de agua municipales</h4>
                </div><!--Agregar grundfos-->
            </div>

        </section>
        <!-- ***** Service Area End ***** -->

        <section class="mosh-more-services-area d-sm-flex clearfix">
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay=".7s">
                    <a id="h5"><img src="imgs/Tratamiento de Agua.png" width="114" height="100"></a>
                    <h4>Tratamiento de agua</h4>
                </div>
            </div>
            <div class="single-more-service-area">
                <div class="more-service-content text-center wow fadeInUp" data-wow-delay="1s">
                    <a id="h6"><img src="imgs/PaqueteHVFDcp.jpg" width="148" height="100"></a>
                    <h4>HVFD</h4>
                </div>
            </div>
        </section>
        <!-- ***** Service Area End ***** -->

        <!-- ***** Welcome Area End ***** -->
        <!--Modal Login-->
        <form id="loginModal" method="POST" action="login">
            <div class="modal fade" id="loginMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Iniciar sesi�n</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div> 
                            <%if (session.getAttribute("mensajeError") != null) {%>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>�Error!</strong> Usuario y/o contrase�a <strong>INCORRECTOS</strong>
                            </div>

                            <%
                                }%>  
                            <%if (session.getAttribute("mensajeError1") != null) {%>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>�Error!</strong> Usuario y/o contrase�a <strong>INCORRECTOS</strong>
                            </div>

                            
                              <%  }%> 
                            <div class="form-group">
                                <label for="nombre" class="control-label">Correo:</label>
                                <input type="email" class="form-control" id="correoModal" name="correoModal" required minlength="8"  maxlength="45">

                            </div>
                            <div class="form-group">
                                <label for="nombre" class="control-label">Contrase�a:</label>
                                <input type="password" class="form-control" id="passModal" name="passModal" required minlength="8" maxlength="45">
                                <div id="error" hidden> 
                                    <div class="alert alert-warning">
                                        <strong>Error</strong> Tu correo (usuario) o contrase�a <a href="#" class="alert-link">Son incorrectos</a>.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-id="log" id="login">Ingresar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--End Modal Login-->


        <!-- ***** Footer Area Start ***** -->
        <footer class="footer-area clearfix">
                <!-- Top Fotter Area -->
                <div class="top-footer-area section_padding_100_0">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <a href="#" class="mb-50 d-block"><img src="imgs/Logo_Incosbo_12.png" alt=""></a>

                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Misi�n</p>
                                    <p>Ser una empresa rentable buscando un crecimiento estable en la comercializaci�n de nuestros productos y servicios, siendo la mejor calificada y reconocida en M�xico por calidad y servicio de nuestro equipo.</p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Visi�n</p>
                                    <p>Consolidarse como una empresa competitiva a nivel nacional, siendo la primera alternativa para nuestros clientes, con el desarrollo de nuestros equipos, manteniendo una filosof�a de mejora continua que nos permita destacar en el sector industrial y del agua</p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/call.png" alt="">
                                        </div>
                                        <p>(442) 5377021 <br>(442) 7130033</p>
                                    </div>
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/message.png" alt="">
                                        </div>
                                        <p>ventas@incosbo.com.mx<br>servicio-incosbo@prodigy.net.mx</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fotter Bottom Area -->
                <div class="footer-bottom-area">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 h-100">
                                <div class="footer-bottom-content h-100 d-md-flex justify-content-between align-items-center">
                                    <div class="copyright-text">
                                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            <a>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                    <div class="footer-social-info">
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        
        <!-- ***** Footer Area End ***** -->

    </body>
    <!-- jQuery-2.2.4 js -->
        <script src="js/jquery-2.2.4.min.js"></script>
        <!-- Popper js -->
        <script src="js/popper.min.js"></script> 
        <!-- Bootstrap js -->
        <script src="js/bootstrap.min.js"></script>       
        <!-- All Plugins js -->
        <script src="js/plugins.js"></script>
        <!-- Active js -->
        <script src="js/active.js"></script>
        
        <script type="text/javascript">
            <%if(session.getAttribute("mensajeError")!=null){%>
                $("#loginMod").modal('show');
            <%  session.removeAttribute("mensajeError");  }%>    
            <%if(session.getAttribute("mensajeError1")!=null){%>
                 $("#loginMod").modal('show');
            <%   session.removeAttribute("mensajeError1");  }%>
        </script>
        <script type="text/javascript">
                var url = "<%=AppHelper.baseUrl()%>productos";
                $("#h1").attr('href', url+"?prod=1");
                $("#h2").attr('href', url+"?prod=5");
                $("#h3").attr('href', url+"?prod=7");
                $("#h4").attr('href', url+"?prod=4");
                $("#h5").attr('href', url+"?prod=3");
                $("#h6").attr('href', url+"?prod=2");
        </script>
</html>