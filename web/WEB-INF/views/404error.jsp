<%-- 
    Document   : 404error
    Created on : 6/06/2018, 10:14:05 AM
    Author     : Negrito
--%>

<%@page import="Helpers.AppHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="imgs/42697fire_98982.ico" />
        <!-- Core Stylesheet -->
        <link href="style.css" rel="stylesheet">
        <!--Responsive CSS-->
        <link href="css/responsive2.css" rel="stylesheet">
        <title>Error</title>
        
    </head>
    <body>
        <div id="preloader">
            <div class="mosh-preloader"></div>
        </div>
        <!-- Header Area Start-->
        <header class="header_area clearfix">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <!-- Menu Area Start-->
                    <div class="col-12 h-100">
                        <div class="menu_area h-100">
                            <nav class="navbar h-100 navbar-expand-lg aling-item-center">
                                <!--Logo-->
                                <a class="navbar-brand" href="<%=AppHelper.baseUrl()%>inicio"><img src="imgs/Logo_Incosbo_12.png" width="180" height="100"></a>

                                <!-- Menu Area-->
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                                <div class="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                                    <ul class="navbar-nav animate" id="nav">
                                        <a class="nav-link" href="<%=AppHelper.baseUrl()%>inicio">Inicio</a>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>conocenos">Conócenos</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>productos">Productos</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>servicios">Servicios</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>contacto">Contacto</a></li>
                                    </ul>
                                    <!--Login-->
                                    <div class="login-register-btn">
                                        <a onclick="#myModal" href=""
                                           data-toggle="modal" data-target="#loginMod">Iniciar sesión</a>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header Area End-->

        <!--Modal Login-->
        <form id="loginModal" method="POST" action="login">
            <div class="modal fade" id="loginMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Iniciar sesión</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>  
                            <%if (session.getAttribute("mensajeError") != null) {%>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>¡Error!</strong> Usuario y/o contraseña <strong>INCORRECTOS</strong>
                            </div>

                            <%              session.removeAttribute("mensajeError");
                                }%> 
                            <%if (session.getAttribute("mensajeError1") != null) {%>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>¡Error!</strong> Usuario y/o contraseña <strong>INCORRECTOS</strong>
                            </div>

                            <%              session.removeAttribute("mensajeError1");
                                }%> 
                            <div class="form-group">
                                <label for="nombre" class="control-label">Correo:</label>
                                <input type="email" class="form-control" id="correoModal" name="correoModal" required minlength="8"  maxlength="45">

                            </div>
                            <div class="form-group">
                                <label for="nombre" class="control-label">Contraseña:</label>
                                <input type="password" class="form-control" id="passModal" name="passModal" required minlength="8"  maxlength="45">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-id="log" id="login">Ingresar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--End Modal Login-->

        <section class="mosh-workflow-area section_padding_100_0 clearfix" style="background-image: url(imgs/welcome-bg.png)">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-heading text-center mb-0">

                            <p style="color: white; font-size: 20px">¡Error! <br> Esta sección no existe <br> Comprueba tu URL</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="mosh-more-services-area d-sm-flex clearfix">
            
        </section>

        <!-- Footer Area Start-->
        <footer class="footer-area clearfix">
            <!-- Top Footer Area-->
            <footer class="footer-area clearfix">
                <!-- Top Footer Area-->
                <div class="top-footer-area section_padding_100_0">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <a href="#" class="mb-50 d-block"><img src="imgs/Logo_Incosbo_12.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Misión</p>
                                    <p>Ser una empresa rentable buscando un crecimiento estable en la comercialización de nuestros productos y servicios, siendo la mejor calificada y reconocida en México por calidad y servicio de nuestro equipo.</p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Visión</p>
                                    <p>Consolidarse como una empresa competitiva a nivel nacional, siendo la primera alternativa para nuestros clientes, con el desarrollo de nuestros equipos, manteniendo una filosofía de mejora continua que nos permita destacar en el sector industrial y del agua</p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/call.png" alt="">
                                        </div>
                                        <p>(442) 5377021 <br>(442) 7130033</p>
                                    </div>
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/message.png" alt="">
                                        </div>
                                        <p>ventas@incosbo.com.mx<br>servicio-incosbo@prodigy.net.mx</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fotter Bottom Area -->
                <div class="footer-bottom-area">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 h-100">
                                <div class="footer-bottom-content h-100 d-md-flex justify-content-between align-items-center">
                                    <div class="copyright-text">
                                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright Colorlib &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                    <div class="footer-social-info">
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- ***** Footer Area End ***** --> 
    </body>
    <!--jQuery-2.2.4 js-->
    <script src="js/jquery-2.2.4.min.js"></script>
    <!-- Popper js-->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js-->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins js-->
    <script src="js/plugins.js"></script>
    <!-- Active js-->
    <script src="js/active.js"></script>
    <script type="text/javascript">
        <%if (session.getAttribute("mensajeError") != null) {%>
              $("#loginMod").modal('show');
        <%    session.removeAttribute("mensajeError");
            }%>
        <%if (session.getAttribute("mensajeError1") != null) {%>
              $("#loginMod").modal('show');
        <%    session.removeAttribute("mensajeError1");
            }%>
    </script>
</html>