<%-- 
    Document   : index
    Created on : 16/05/2018, 09:24:35 AM
    Author     : Negrito
--%>

<%@page import="Helpers.AppHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        
        <meta name="google-site-verification" content="hQqcfoVcg4_A5I-lmgabvhHTWCYdLPzR9-GFVdfuGI4" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%response.sendRedirect(AppHelper.baseUrl()+AppHelper.defaultController());%>
    </body>
</html>
