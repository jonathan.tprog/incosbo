<%-- 
    Document   : productosAdmin
    Created on : 17/05/2018, 12:42:07 PM
    Author     : Negrito
--%>

<%@page import="Entity.Productos"%>
<%@page import="java.util.List"%>
<%@page import="Helpers.AppHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="imgs/42697fire_98982.ico" />
        <!-- Core Stylesheet -->
        <link href="style.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
        <title>INCOSBO</title>
    </head>
<body onload="denegado();">
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="mosh-preloader"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area clearfix">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <!-- Menu Area Start -->
                <div class="col-12 h-100">
                    <div class="menu_area h-100">
                        <nav class="navbar h-100 navbar-expand-lg align-items-center">
                            <!-- Logo -->
                            <a class="navbar-brand" href="<%=AppHelper.baseUrl()%>adminProductos"><img src="imgs/Logo_Incosbo_12.png" width="180" height="100" alt="logo"></a>

                            <!-- Menu Area -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                            <div class="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                                <ul class="navbar-nav animated" id="nav">
                                    <a class="nav-link" href="<%=AppHelper.baseUrl()%>adminProductos">Inicio</a>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminProductos">Productos</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminServicios">Servicios</a></li>
                                    <%HttpSession sesiones;
                                    sesiones = request.getSession(false);
                                    if(AppHelper.tieneAcceso(sesiones, 1)){%>
                                    <li class="nav-item"><a class="nav-link" href="<%=AppHelper.baseUrl()%>adminUsuarios">Usuarios</a></li>
                                    <%}%>
                                </ul>
                                <!-- Login-->
                                <div class="login-register-btn">
                                    <a onclick="" href="<%=AppHelper.baseUrl()%>logout"
                                       data-toggle="" data-target="">Cerrar sesión</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    <!-- 
    Rojo-- contra incendio
    Azul--- hidrosanitarios
    -->
    
    
        <!--Modal add-->
        <form id="addModal" action="nuevoProducto" method="POST" enctype="multipart/form-data" accept-charset="UTF-8">
            <div class="modal fade" id="addMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Agregar nuevo producto</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>    
                            <div class="form-group">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="nombreModal" name="nombreModal" required maxlength="1500">

                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="control-label">Descripción</label>
                                <input type="text" class="form-control" id="desModal" name="desModal" required maxlength="1500">
                            </div>
                            <div class="form-group">
                                <label for="imagen" class="control-label">Imagen</label>
                                <input type="file" class="form-control" id="imgModal" name="imgModal"  required accept="image/*">
                            </div>
                            <div class="form-group">
                                <label for="doc" class="control-label">Documento</label>
                                <input type="file" class="form-control" id="docModal" name="docModal" required maxlength="45">
                            </div>
                            <div class="form-group">
                                <label for="cate" class="control-label">Categoria</label>
                                <select id="eleccion"class="form-control" name="eleccion">
                                    <option value="1">Hidrosanitarias</option>
                                    <option value="2">HVAC</option>
                                    <option value="3">Tratamiento de aguas</option>
                                    <option value="4">Pozos y redes de agua municipales</option>
                                    <option value="5">Aguas pluviales</option>
                                    <option value="6">Aplicaciones especiales</option>
                                    <option value="7">Contra Incendio</option>
                                    <option value="8">GRUNDFOS</option>
                                    <option value="9">GRUNDFOS---Sumergibles</option>
                                    <option value="10">GRUNDFOS---Industriales</option>
                                    <option value="11">GRUNDFOS---Resirculadores</option>
                                    <option value="12">GRUNDFOS---Water Waste</option>
                                    <option value="13">GRUNDFOS---Doméstico</option>
                                    <option value="14">GRUNDFOS---Hydro</option>
                                    <option value="15">GRUNDFOS---Dosing</option>
                                    <option value="16">GRUNDFOS---PACO---Carcasa bipartida</option>
                                    <option value="17">GRUNDFOS---PACO---Horizontales</option>
                                    <option value="18">GRUNDFOS---PACO---Verticales</option>
                                    <option value="19">GRUNDFOS---PEERLEES---Carcasa bipartida</option>
                                    <option value="20">GRUNDFOS---PEERLEES---ENDSUTION</option>
                                    <option value="21">GRUNDFOS---PEERLEES---VTP</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-id="log">Agregar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--End Modal add-->

        <!--Modal edit-->
        <form id="EditModal" action="editProd" method="POST" enctype="multipart/form-data">
            <div class="modal fade" id="editModP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Actualizar producto</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>    
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="idEdit" name="idEdit" required maxlength="45">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="nombreEdit" name="nombreEdit" required maxlength="1500">
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="control-label">Descripción</label>
                                <input type="text" class="form-control" id="desEdit" name="desEdit" required  maxlength="1500">
                            </div>
                            <div class="form-group">
                                <label for="imagen" class="control-label">Imagen</label>
                                <input type="file" class="form-control" id="imgEdit" name="imgEdit"  accept="image/*">
                                <input type="text" class="form-control" id="imgEditRes" name="imgEditRes" disabled>
                            </div>
                            <div class="form-group">
                                <label for="doc" class="control-label">Documento</label>
                                <input type="file" class="form-control" id="docEdit" name="docEdit" maxlength="45">
                                <input type="text" class="form-control" id="docEditRes" name="docEditRes" disabled>
                            </div>
                            <div class="form-group">
                                <label for="cate" class="control-label">Categoria</label>
                                <select id="eleccionEdit"class="form-control" name="eleccionEdit">
                                    <option value="1">Hidrosanitarias</option>
                                    <option value="2">HVAC</option>
                                    <option value="3">Tratamiento de aguas</option>
                                    <option value="4">Pozos y redes de agua municipales</option>
                                    <option value="5">Aguas pluviales</option>
                                    <option value="6">Aplicaciones especiales</option>
                                    <option value="7">Contra Incendio</option>
                                    <option value="8">GRUNDFOS</option>
                                    <option value="9">GRUNDFOS---Sumergibles</option>
                                    <option value="10">GRUNDFOS---Industriales</option>
                                    <option value="11">GRUNDFOS---Resirculadores</option>
                                    <option value="12">GRUNDFOS---Water Waste</option>
                                    <option value="13">GRUNDFOS---Doméstico</option>
                                    <option value="14">GRUNDFOS---Hydro</option>
                                    <option value="15">GRUNDFOS---Dosing</option>
                                    <option value="16">GRUNDFOS---PACO---Carcasa bipartida</option>
                                    <option value="17">GRUNDFOS---PACO---Horizontales</option>
                                    <option value="18">GRUNDFOS---PACO---Verticales</option>
                                    <option value="19">GRUNDFOS---PEERLEES---Carcasa bipartida</option>
                                    <option value="20">GRUNDFOS---PEERLEES---ENDSUTION</option>
                                    <option value="21">GRUNDFOS---PEERLEES---VTP</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-id="log">Editar</button>
                        </div>
                    </div>
                </div>
            </div>
            
        </form>
        <!--End Modal edit-->

    <section class="mosh-workflow-area section_padding_100_0 clearfix" id="home" style="background-image: url(imgs/welcome-bg.png)">
        <div class="container">

            <div class="row"> 
                <div class="col-sm-12" align="center">
                    <button class="btn btn-group" align="center" onClick="#myModal" href="" data-toggle="modal" data-target="#addMod">Agregar nuevo producto</button> 
                </div>
                &nbsp;
                <div class="col-sm-12" align="center">
                    <%if (session.getAttribute("mensajeOk") != null) {%>
                    <div class="col-sm-12">
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>¡Correcto!</strong> El producto fue registrado
                        </div>
                    </div>
                    <% session.removeAttribute("mensajeOk");
                        }%>
                    <%if (session.getAttribute("mensajeError") != null) {%>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>¡Error!</strong> El producto ya está registrado
                    </div>
                    <%session.removeAttribute("mensajeError");
                        }%>
                </div>
                &nbsp;
                <div class="table-responsive">
                    <%List<Productos> listaP = (List<Productos>) request.getAttribute("busqueda");%>

                    <table class= "table table-bordered table-dark" > 
                        <%if (listaP != null) {
                                for (Productos prod : listaP) {%>

                        <thead> 
                            <tr> 
                                <th scope= "col" > Nombre </th>
                                <th scope= "col" > Descripción </th>
                                <th scope= "col" > Imagen </th>
                                <th scope= "col" > Documento </th>
                                <th scope= "col" > Categoria </th>
                                <th scope= "col" > Acción </th>
                            </tr> 
                        </thead>
                        <tbody> 
                            <tr>
                                <th scope= "row" ><%=prod.getNombre()%></th> 
                                <th scope= "row" ><%=prod.getDescripcion()%></th> 
                                <th scope= "row" ><img src="imgServer/<%=prod.getImagen()%>" width="80" height="80"</th> 
                                <th scope= "row" ><a href="docServer/<%=prod.getDescDoc()%>">Abrir archivo</a></th> 
                                <th scope= "row" ><%=prod.getIdCat().getCategoria()%></th> 
                                <th scope= "row" >
                                    <button type="button" 
                                            class="btn btn-info" 
                                            data-id="<%=prod.getIdProd()%>"
                                            data-nombre="<%=prod.getNombre()%>"
                                            data-desc="<%=prod.getDescripcion()%>"
                                            data-cat="<%=prod.getIdCat().getIdCat()%>"
                                            data-img="<%=prod.getImagen()%>"
                                            data-doc="<%=prod.getDescDoc()%>"
                                            onClick="#myModal" 
                                            href="" 
                                            data-toggle="modal" 
                                            data-target="#editModP">
                                        Editar
                                    </button>
                                    <button type="button" 
                                            class="mode btn btn-danger"
                                            data-id="<%=prod.getIdProd()%>">
                                        Eliminar
                                    </button>
                                </th> 
                                <%}
                                    }%>
                            </tr>
                        </tbody>
                    </table>
                </div>
             
        &nbsp;
        &nbsp;
         </div>
        </div> 
        
    </section>

         <!-- ***** Footer Area Start ***** -->
        <footer class="footer-area clearfix">
                <!-- Top Fotter Area -->
                <div class="top-footer-area section_padding_100_0">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <a href="#" class="mb-50 d-block"><img src="imgs/Logo_Incosbo_12.png" alt=""></a>

                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Misión</p>
                                    <p>Ser una empresa rentable buscando un crecimiento estable en la comercialización de nuestros productos y servicios, siendo la mejor calificada y reconocida en México por calidad y servicio de nuestro equipo.</p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <p>Visión</p>
                                    <p>Consolidarse como una empresa competitiva a nivel nacional, siendo la primera alternativa para nuestros clientes, con el desarrollo de nuestros equipos, manteniendo una filosofía de mejora continua que nos permita destacar en el sector industrial y del agua</p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-footer-widget mb-100">
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/call.png" alt="">
                                        </div>
                                        <p>(442) 5377021 <br>(442) 7130033</p>
                                    </div>
                                    <div class="footer-single-contact-info d-flex">
                                        <div class="contact-icon">
                                            <img src="imgs/message.png" alt="">
                                        </div>
                                        <p>ventas@incosbo.com.mx<br>servicio-incosbo@prodigy.net.mx</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fotter Bottom Area -->
                <div class="footer-bottom-area">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 h-100">
                                <div class="footer-bottom-content h-100 d-md-flex justify-content-between align-items-center">
                                    <div class="copyright-text">
                                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            <a>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                    <div class="footer-social-info">
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
   
        <!-- ***** Footer Area End ***** -->
        <%request.removeAttribute("busqueda");%>
</body>
<!-- jQuery-2.2.4 js -->
        <script src="js/jquery-2.2.4.min.js"></script>
        <!-- Popper js -->
        <script src="js/popper.min.js"></script> 
        <!-- Bootstrap js -->
        <script src="js/bootstrap.min.js"></script>       
        <!-- All Plugins js -->
        <script src="js/plugins.js"></script>
        <!-- Active js -->
        <script src="js/active.js"></script>
        
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.mode').click(function () {
            var conf = confirm("¿Estas seguro que quieres elimnar a este producto?");
            if (conf) {
                $.ajax({
                    url: "delProd",
                    type: "POST",
                    data: {
                        producto: $(this).attr("data-id")
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function () {}
                });
            }
        });
    });
</script>

<script>
    $('#editModP').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var nombre = button.data('nombre');
        var desc = button.data('desc');
        var img = button.data('img');
        var doc = button.data('doc');
        var cat = button.data('cat');

        var modal = $(this);
        modal.find('.modal-body #idEdit').val(id);
        modal.find('.modal-body #nombreEdit').val(nombre);
        modal.find('.modal-body #desEdit').val(desc);
        modal.find('.modal-body #imgEditRes').val(img);
        modal.find('.modal-body #docEditRes').val(doc);
        modal.find('.modal-body #eleccionEdit').val(cat);
    });
</script>
<script>
        function denegado(){
            window.location.hash="no-back-button";
            window.location.hash="Again-No-back-button" //chrome
            window.onhashchange=function(){window.location.hash="no-back-button";}
        }
</script>
</html>