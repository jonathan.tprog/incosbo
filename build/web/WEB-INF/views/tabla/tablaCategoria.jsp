<%-- 
    Document   : tablaCategoria
    Created on : 16/05/2018, 04:40:06 PM
    Author     : Negrito
--%>

<%@page import="java.util.List"%>
<%@page import="Entity.Productos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!-- Consu ltas de productos -->
<!-- ***** Preloader Start ***** -->
&nbsp;
<!--Muestra cada una de las productos registradas-->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel-default">
                <div class="container">
                    <table class= "table table-bordered table-dark"> 
                        <tr>
                            <th>Nombre</th>
                            <th>Imagen / Saber más</th>
                            <th>Categoria</th>
                        </tr>
                        <%List<Productos> listaFiltro = (List<Productos>) request.getAttribute("listaFiltro");
                                if (listaFiltro != null) {%>
                        <%for (Productos productos : listaFiltro) {%>                  

                        <tr>
                            <td><%=productos.getNombre()%></td>
                            <td><img
                                        data-descripcion="<%=productos.getDescripcion()%>"
                                        data-img="<%=productos.getImagen()%>"
                                        data-nombre="<%=productos.getNombre()%>"
                                        data-doc="<%=productos.getDescDoc()%>"
                                        onClick="#myModal" 
                                        href="" 
                                        data-toggle="modal" 
                                        data-target="#infoMod"
                                        src="imgServer/<%=productos.getImagen()%>" style="width: 100px; height: 100px; display: block;">
                                
                            </td>
                            <td><%=productos.getIdCat().getCategoria()%></td>
                        </tr>
                        <%}
                                }%>
                    </table>
               </div>
            </div>
        </div>
    </div>
</div>


<!--Modal info-->
<form id="infoModal">
    <div class="modal fade" id="infoMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="section-heading text-center mb-0">
                        <img id="imagen" src="" width="200" height="200">
                        <h1 style="font-size: 18px;">Descripción:</h1>
                    </div>
                    <div class="modal-body1">
                        <p id="descripcion" style="font-size: 15px; color: gray;"></p>
                    </div>


                </div>
                <div class="modal-footer">
                    <a id="lanzar" href=""><input type="button" id="buscar" class="btn btn-success" style="width: 110px; height: 40px;" value="Descripción"></a>
                </div>
            </div>
        </div>
    </div>
</form>
<!--End Modal info-->

<!-- jQuery-2.2.4 js -->
<script src="js/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/popper.min.js"></script> 
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>       
<!-- All Plugins js -->
<script src="js/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function ($) {});
        $('#infoMod').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var nombre = button.data('nombre');
            var desc = button.data('descripcion');
            var img = button.data('img');
            var doc = button.data('doc');
            var url = 'imgServer/' + img;
            var urlDoc = 'docServer/' + doc;
            var modal = $(this);
            modal.find('.modal-title').text(nombre);
            modal.find('.modal-body #imagen').attr('src', url);
            modal.find('.modal-body1').text(desc);
            modal.find('.modal-footer #lanzar').attr('href', urlDoc);
        });
</script>
