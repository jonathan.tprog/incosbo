package Entity;

import Entity.Categoriaproducto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-03-26T22:37:28")
@StaticMetamodel(Productos.class)
public class Productos_ { 

    public static volatile SingularAttribute<Productos, String> descripcion;
    public static volatile SingularAttribute<Productos, Integer> idProd;
    public static volatile SingularAttribute<Productos, String> imagen;
    public static volatile SingularAttribute<Productos, String> descDoc;
    public static volatile SingularAttribute<Productos, String> nombre;
    public static volatile SingularAttribute<Productos, Categoriaproducto> idCat;
    public static volatile SingularAttribute<Productos, Integer> status;

}