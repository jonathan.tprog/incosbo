package Entity;

import Entity.Categoriaservicio;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-03-26T22:37:28")
@StaticMetamodel(Servicio.class)
public class Servicio_ { 

    public static volatile SingularAttribute<Servicio, String> descripcion;
    public static volatile SingularAttribute<Servicio, Categoriaservicio> idcatSer;
    public static volatile SingularAttribute<Servicio, Integer> idSer;
    public static volatile SingularAttribute<Servicio, String> imagen;
    public static volatile SingularAttribute<Servicio, String> descDoc;
    public static volatile SingularAttribute<Servicio, String> nombre;
    public static volatile SingularAttribute<Servicio, Integer> status;

}