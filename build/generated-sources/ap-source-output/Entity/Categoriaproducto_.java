package Entity;

import Entity.Productos;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-03-26T22:37:28")
@StaticMetamodel(Categoriaproducto.class)
public class Categoriaproducto_ { 

    public static volatile SingularAttribute<Categoriaproducto, String> categoria;
    public static volatile ListAttribute<Categoriaproducto, Productos> productosList;
    public static volatile SingularAttribute<Categoriaproducto, Integer> idCat;
    public static volatile SingularAttribute<Categoriaproducto, Integer> status;

}