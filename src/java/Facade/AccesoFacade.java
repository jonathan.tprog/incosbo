/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Acceso;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Negrito
 */
@Stateless
public class AccesoFacade extends AbstractFacade<Acceso> {

    @PersistenceContext(unitName = "ProyectoIncosboPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccesoFacade() {
        super(Acceso.class);
    }

    public List<Acceso> usuario(){
        em.flush();
        try{
            //-----------------------Esta linea es de GOKU!!!! <3 <3------------------------
            em.getEntityManagerFactory().getCache().evictAll();                        //  |
            //-----------------------Nunca olvidar esta liea de código----------------------
            Query q;
            q=em.createQuery("Select A from Acceso a where a.status<>2");
            System.out.print(q);
            return q.getResultList();
        }catch(Exception e){
            return null;
        }
    }
    
    public Acceso login(String email){
        try{
            return (Acceso) em.createQuery("Select A FROM Acceso  a where a.email=:email and a.status<>2").setParameter("email", email).getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
    
    public boolean actualizar(int idUsu, String email, String password, int tipUsu, int status){
        try{
            Query q = em.createNativeQuery("UPDATE acceso SET email = ?, password=?, tip_usu=?, status=? WHERE idUsu = ?");
            q.setParameter(1, email);
            q.setParameter(2, password);
            q.setParameter(3, tipUsu);
            q.setParameter(4, status);
            q.setParameter(5, idUsu);
            em.flush();
            q.executeUpdate();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean borrar(int clave){
        try{
            Query q = em.createNativeQuery("Update acceso set status=2 where idUsu=?");
            q.setParameter(1, clave);
            q.executeUpdate();
            em.flush();
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public Acceso validaCorreo(String email){
        try{
            return (Acceso) em.createQuery("Select A from Acceso a where a.email=:email and a.status<>2").setParameter("email", email).getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
    
}


