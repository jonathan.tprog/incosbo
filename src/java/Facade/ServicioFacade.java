/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Servicio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Negrito
 */
@Stateless
public class ServicioFacade extends AbstractFacade<Servicio> {

    @PersistenceContext(unitName = "ProyectoIncosboPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServicioFacade() {
        super(Servicio.class);
    }
    
public List<Servicio> listaServicios(){
        em.flush();
        try{
              em.getEntityManagerFactory().getCache().evictAll();   
              Query q = em.createQuery("SELECT S from Servicio s where s.status<>2 order by s.idcatSer.servicio ASC");
            return q.getResultList();
        }catch(Exception e){
            return null;
        }
    }

 public Servicio noImg(int idSer){
        try{
            return (Servicio) em.createQuery("Select P from Servicio p where p.idSer=:idSer").setParameter("idSer", idSer).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public boolean eliminarS(int clave){
        try{
            Query q = em.createNativeQuery("Update servicio set status=2 where id_ser=?");
            q.setParameter(1, clave);
            q.executeUpdate();
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public Servicio validarServicio(String nombre){
        try{
            return (Servicio) em.createQuery("Select S from Servicio s where s.nombre=:nombre and s.status<>2").setParameter("nombre", nombre).getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
    
    
    public List<Servicio> listaFiltroSer(int idcatSer){
        try{
            em.getEntityManagerFactory().getCache().evictAll();
            Query q = em.createQuery("Select S from Servicio s where s.status<>2 and s.idcatSer.idcatSer=:idcatSer order by s.idcatSer.servicio ASC");
            q.setParameter("idcatSer", idcatSer);
            return q.getResultList();
        }catch(Exception e){
            return null;
        }
    }
}

