/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Categoriaproducto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Negrito
 */
@Stateless
public class CategoriaproductoFacade extends AbstractFacade<Categoriaproducto> {

    @PersistenceContext(unitName = "ProyectoIncosboPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoriaproductoFacade() {
        super(Categoriaproducto.class);
    }
    
}
