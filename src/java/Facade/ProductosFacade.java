/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Productos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Negrito
 */
@Stateless
public class ProductosFacade extends AbstractFacade<Productos> {

    @PersistenceContext(unitName = "ProyectoIncosboPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductosFacade() {
        super(Productos.class);
    }
    
    public List<Productos> listaProductos(){
        em.flush();
        try{
            em.getEntityManagerFactory().getCache().evictAll();                        
            Query q =em.createQuery("Select P from Productos p where p.status<>2 order by p.idCat.categoria ASC");
            return q.getResultList();
        }catch(Exception e){
            return null;
        }
    }
    
    public Productos noImg(int idProd){
        try{
            return (Productos) em.createQuery("Select P from Productos p where p.idProd=:idProd").setParameter("idProd", idProd).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public boolean eliminarP(int clave){
    try{
        Query q = em.createNativeQuery("Update productos set status=2 where id_prod=?");
        q.setParameter(1, clave);
        q.executeUpdate();
        return true;
    }catch(Exception e){
        return false;
    }
    }
    
    public Productos validarProducto(String nombre){
        try{
            return (Productos) em.createQuery("Select P from Productos p where p.nombre=:nombre and p.status<>2").setParameter("nombre", nombre).getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
    
    public List<Productos> listaFiltro(int idCat){
        try{
            em.getEntityManagerFactory().getCache().evictAll();
            Query q = em.createQuery("Select P from Productos p where p.status<>2 and p.idCat.idCat=:idCat order by p.idCat.categoria ASC");
            q.setParameter("idCat", idCat);
            return q.getResultList();
        }catch(Exception e){
            return null;
        }
    }
    
}
    

