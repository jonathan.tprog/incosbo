/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entity.Acceso;
import Entity.Productos;
import Entity.Servicio;
import Facade.AccesoFacade;
import Facade.ProductosFacade;
import Facade.ServicioFacade;
import Helpers.AppHelper;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Negrito
 */
@WebServlet(name = "ctrlAdmin", urlPatterns = {"/adminProductos", "/adminServicios", "/adminUsuarios", "/addUsu", "/verUsus", "/editUsu", "/deliUsu"})
public class ctrlAdmin extends HttpServlet {

    @EJB
    private AccesoFacade ac;
    @EJB
    private ProductosFacade pc;
    @EJB
    private ServicioFacade sf;
    HttpSession sesiones;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sesiones =  request.getSession(false);
        System.out.println("HOOOOOOLLAAAAAAA "+request.getParameter("tpUsu"));
        if (AppHelper.tieneAcceso(sesiones, 1)) {
            switch (request.getServletPath()) {
                case "/adminProductos":
                    List<Productos> listaP = pc.listaProductos();
                    request.setAttribute("busqueda", listaP);
                    request.getRequestDispatcher("WEB-INF/views/admins/productosAdmin.jsp").forward(request, response);
                    break;
                case "/adminServicios":
                    List<Servicio> listaS = sf.listaServicios();
                    request.setAttribute("busqueda", listaS);
                    request.getRequestDispatcher("WEB-INF/views/admins/serviciosAdmin.jsp").forward(request, response);
                    break;
                case "/adminUsuarios":
                    List<Acceso> lista = ac.usuario();
                    request.setAttribute("busqueda", lista);
                    request.getRequestDispatcher("WEB-INF/views/admins/usuariosAdmin.jsp").forward(request, response);
                    lista.clear();
                    break;
                default:
                    request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                    break;

            }
        } else {
            if (AppHelper.tieneAcceso(sesiones, 2)) {
                System.out.println(sesiones.getAttribute("tpUsu").equals(2));
                switch (request.getServletPath()) {
                    case "/adminProductos":
                        try {
                            List<Productos> listaP = pc.listaProductos();
                            request.setAttribute("busqueda", listaP);
                            request.getRequestDispatcher("WEB-INF/views/admins/productosAdmin.jsp").forward(request, response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "/adminServicios":
                        List<Servicio> listaS = sf.listaServicios();
                        request.setAttribute("busqueda", listaS);
                        request.getRequestDispatcher("WEB-INF/views/admins/serviciosAdmin.jsp").forward(request, response);
                        break;
                    case "/adminUsuarios":
                        List<Productos> listaP = pc.listaProductos();
                        request.setAttribute("busqueda", listaP);
                        request.getRequestDispatcher("WEB-INF/views/admins/productosAdmin.jsp").forward(request, response);
                        break;
                    default:
                        request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                        break;
                }
            } else {
                response.sendRedirect(AppHelper.baseUrl() + "inicio");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sesiones =  request.getSession(false);
        if (AppHelper.tieneAcceso(sesiones, 1)) {
            switch (request.getServletPath()) {
                case "/addUsu":
                    Acceso acceso = new Acceso();
                    acceso.setEmail(request.getParameter("emailModal"));
                    acceso.setPassword(AppHelper.convertirMD5(request.getParameter("passModal")));
                    acceso.setTipUsu(Integer.parseInt(request.getParameter("usuario")));
                    acceso.setStatus(1);
                    if (ac.validaCorreo(acceso.getEmail()) != null) {
                        sesiones.setAttribute("mensajeError", "existe");
                        System.out.println("Ya existe");
                    } else {
                        sesiones.setAttribute("mensajeOk", "Noexiste");
                        ac.create(acceso);
                    }
                    response.sendRedirect(AppHelper.baseUrl() + "adminUsuarios");
                    break;
                case "/editUsu":
                    int id = Integer.parseInt(request.getParameter("idEdit"));
                    String email = request.getParameter("emailEdit");
                    String password = AppHelper.convertirMD5(request.getParameter("passEdit"));
                    int usu = Integer.parseInt(request.getParameter("usuEdit"));
                    int status = 1;
                    if (ac.validaCorreo(email) != null) {
                        sesiones.setAttribute("mensajeError", "existe");
                        System.out.println("Ya existe");
                        //Si el usuario existe, pero solo quiere cambiar password o tipo (no correo)
                        if (ac.validaCorreo(email).getEmail().equals(email)) {
                            sesiones.setAttribute("mensajeOk", "Noexiste");
                            sesiones.removeAttribute("mensajeError");
                            ac.actualizar(id, email, password, usu, status);
                            if(sesiones.getAttribute("correo").equals(email)){
                                if (usu == 1) {
                                response.sendRedirect(AppHelper.baseUrl() + "adminUsuarios");
                                sesiones.setAttribute("tpUsu", 1);
                            } else {
                                sesiones.removeAttribute("mensajeOk");
                                if (usu == 2) {
                                    response.sendRedirect(AppHelper.baseUrl() + "adminProductos");
                                    sesiones.setAttribute("tpUsu", 2);
                                }
                            }
                            }
                            response.sendRedirect(AppHelper.baseUrl() + "adminUsuarios");
                            
                        }
                    } else {
                        sesiones.setAttribute("mensajeOk", "Noexiste");
                        ac.actualizar(id, email, password, usu, status);
                        if (usu == 1) {
                            response.sendRedirect(AppHelper.baseUrl() + "adminUsuarios");
                            sesiones.setAttribute("tpUsu", 1);
                        } else {
                            if (usu == 2) {
                                sesiones.removeAttribute("mensajeOk");
                                response.sendRedirect(AppHelper.baseUrl() + "adminProductos");
                                sesiones.setAttribute("tpUsu", 2);
                            }
                        }
                    }
                    break;
                case "/deliUsu":
                    ac.borrar(Integer.parseInt(request.getParameter("usuario")));
                    response.sendRedirect(AppHelper.baseUrl() + "adminUsuarios");
                    break;
                default:
                    request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                    break;
            }
        } else {
            response.sendRedirect(AppHelper.baseUrl() + "inicio");
        }
    }
}