/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Entity.Productos;
import Entity.Servicio;
import Facade.ProductosFacade;
import Facade.ServicioFacade;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Negrito
 */
@WebServlet(name = "ctrlPrincipal", urlPatterns = {"/inicio", "/productos", "/contacto", "/servicios", "/conocenos", "/cargar", "/cargarS", "/cargarSer", "/cargarServicio", "/cargarP"})
public class ctrlPrincipal extends HttpServlet {

    int clave;
    @EJB
    private ProductosFacade pf;
    @EJB
    private ServicioFacade sf;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/inicio":
                request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                break;
            case "/productos":
                int prod;
                String pr = request.getParameter("prod");
                if(pr != null){
                    prod = Integer.parseInt(request.getParameter("prod"));
                }else{
                    prod=0;
                }
                request.setAttribute("producto", prod);
                request.getRequestDispatcher("WEB-INF/views/productos.jsp").forward(request, response);
                break;
            case "/contacto":
                request.getRequestDispatcher("WEB-INF/views/contacto.jsp").forward(request, response);
                break;
            case "/servicios":
                request.getRequestDispatcher("WEB-INF/views/mantenimiento.jsp").forward(request, response);
                break;
            case "/conocenos":
                request.getRequestDispatcher("WEB-INF/views/conocenos.jsp").forward(request, response);
                break;
            case "/cargarP":
                clave = Integer.parseInt(request.getParameter("idCat"));
                break;
            case "/cargar":
                List<Productos> listaFiltro = pf.listaFiltro(clave);
                request.setAttribute("listaFiltro", listaFiltro);
                request.getRequestDispatcher("WEB-INF/views/tabla/tablaCategoria.jsp").forward(request, response);
                break;
            case "/cargarS":
                clave = Integer.parseInt(request.getParameter("idSer"));
                break;
            case "/cargarServicio":
                List<Servicio> listaFiltroSer = sf.listaFiltroSer(clave);
                request.setAttribute("listaFiltro", listaFiltroSer);
                request.getRequestDispatcher("WEB-INF/views/tabla/tablaCategoriaServicio.jsp").forward(request, response);
                break;
            default:
                request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}