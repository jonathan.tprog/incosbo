/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Entity.Acceso;
import Facade.AccesoFacade;
import Helpers.AppHelper;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Negrito
 */

@WebServlet(name = "ctrlLogin", urlPatterns = {"/login", "/logout"})
public class ctrlLogin extends HttpServlet  implements  Serializable {

    
    
    @EJB
    private AccesoFacade ac;
    HttpSession sesiones;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/logout":
                this.sesiones =  request.getSession(false);
                System.err.println("Entré al cerrar sesion");
                sesiones.invalidate();
                request.getRequestDispatcher("index.jsp").forward(request, response);
        break;
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            switch (request.getServletPath()) {
            case "/login":
                HttpSession sesiones =  request.getSession(false);
                Acceso acc = new Acceso();
                String correo = request.getParameter("correoModal");
                acc = ac.login(correo);
                if (acc != null) {
                    String password = AppHelper.convertirMD5(request.getParameter("passModal"));
                    if (acc.getPassword().equals(password)) {
                        System.out.println("Contraseña válida "+acc.getTipUsu());

                        sesiones.setAttribute("tpUsu", acc.getTipUsu());
                        sesiones.setAttribute("correo", acc.getEmail());

                        System.out.println("Pasé las sesiones " + sesiones.getAttribute("tpUsu"));

                        response.sendRedirect(AppHelper.baseUrl() + "adminProductos");
                    } else {
                        sesiones.setAttribute("mensajeError", "existe");
                        
                        response.sendRedirect(AppHelper.baseUrl() + "inicio");
                    }
                } else {
                    sesiones.setAttribute("mensajeError1", "nulo");
                    response.sendRedirect(AppHelper.baseUrl() + "inicio");
                }
                break;
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}