/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Helpers.AppHelper;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeBodyPart;
import java.io.IOException;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Negrito
 */
@WebServlet(name = "ctrlCorreo", urlPatterns = {"/enviarCorreo"})
public class ctrlCorreo extends HttpServlet {
    
    private HttpSession sesiones;
   
    String nombre = "";
    String correo = "sitio.web.incosbo@gmail.com";
    String Destino = "sitio.web.incosbo@gmail.com";
    String UsuarioCorreo="";
    String pass = "incosbo1973";
    String numero="";
    String mensajeI = "";
    String mensajeUsu="";
    String asuntoI = "Correo enviado desde el sitio web incosbo.com";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        

    } 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException { 
         switch (request.getServletPath()) {
            case "/enviarCorreo":
                try{
                    this.sesiones = request.getSession(false);
                    nombre=(new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8"));
                    UsuarioCorreo=request.getParameter("email");
                    mensajeUsu=(new String(request.getParameter("comentario").getBytes("ISO-8859-1"), "UTF-8"));
                    numero = request.getParameter("numero");
                   System.out.println("Va bien");
                   mensajeI = "Se ha recibido un comentario desde el sitio web con los siguientes datos:\nNombre: "+nombre+"\nCorreo: "+UsuarioCorreo+"\nNúmero: "+numero+"\nMensaje:\n"+mensajeUsu;
                   if(envio()){
                       System.out.println("Exito");
                       sesiones.setAttribute("envio", "enviado");
                       System.out.println(sesiones.getAttribute("envio"));
                       response.sendRedirect(AppHelper.baseUrl() + "contacto");
                   }
                   else{
                       sesiones.setAttribute("envioE", "enviadoNo");
                       response.sendRedirect(AppHelper.baseUrl() + "contacto");
                   }
                }catch(Exception e){
                    e.printStackTrace();
                }
            break;
        }
        
    }
    
    public boolean envio() {
        try {
            Properties p = new Properties();
            p.put("mail.smtp.host", "smtp.gmail.com");
            p.setProperty("mail.smtp.starttls.enable", "true");
            p.setProperty("mail.smtp.port", "587");
            p.setProperty("mail.smtp.user", "sitio.web.incosbo@gmail.com");
            p.setProperty("mail.smpt.auth", "true");

            Session s = Session.getDefaultInstance(p);
            BodyPart texto = new MimeBodyPart();
            texto.setText(mensajeI);
            BodyPart asunto = new MimeBodyPart();
            asunto.setText(asuntoI);
            System.out.println("En envio");
            
            MimeMultipart m = new MimeMultipart();
            m.addBodyPart(texto);
            
            MimeMessage mensaje = new MimeMessage(s);
            mensaje.setFrom(new InternetAddress(correo));
            mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(Destino));
            mensaje.setSubject(asuntoI);
            mensaje.setContent(m);
            
            Transport t = s.getTransport("smtp");
            t.connect(correo, pass);
            t.sendMessage(mensaje, mensaje.getAllRecipients());
            t.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}