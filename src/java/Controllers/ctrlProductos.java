/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Entity.Categoriaproducto;
import Entity.Productos;
import Facade.ProductosFacade;
import Helpers.AppHelper;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Negrito
 */
@WebServlet(name = "ctrlProductos", urlPatterns = {"/nuevoProducto", "/editProd", "/delProd"})
public class ctrlProductos extends HttpServlet {

    @EJB
    private ProductosFacade pc;
    HttpSession sesiones;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sesiones = (HttpSession) request.getSession();
        if (AppHelper.tieneAcceso(this.sesiones, 1) || AppHelper.tieneAcceso(this.sesiones, 2)) {
            switch (request.getServletPath()) {
                case "/nuevoProducto":
                    boolean bandera = false;
                    request.setCharacterEncoding("UTF-8");
                    System.out.println("En nuevo producto");
                    try {
                        //Subir archivos
                        Productos productos = new Productos();
                        List<FileItem> multiparts;
                        multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (item.isFormField()) {
                                if (item.getFieldName().equals("nombreModal")) {
                                    //Caracteres especiales 
                                    productos.setNombre(new String(item.getString().getBytes("ISO-8859-1"),"UTF-8"));
                                    
                                }
                                if (item.getFieldName().equals("desModal")) {
                                    productos.setDescripcion(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("eleccion")) {
                                    productos.setIdCat(new Categoriaproducto());
                                    productos.getIdCat().setIdCat(Integer.parseInt(item.getString()));
                                }
                            } else {
                                String rutaImg;
                                String nombreImg = new File(item.getName()).getName();
                                if (bandera == false) {
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/imgServer/";
                                    request.setAttribute("imgModal", nombreImg);
                                } else {
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/docServer/";
                                    request.setAttribute("docModal", nombreImg);
                                }
                                File nuevaImagen = new File(rutaImg + nombreImg);
                                System.out.println("Imagen: " + nuevaImagen);
                                item.write(nuevaImagen);
                                if (bandera == false) {
                                    productos.setImagen(nombreImg);
                                } else {
                                    productos.setDescDoc(nombreImg);
                                }

                                bandera = true;
                            }
                        }
                        productos.setStatus(1);
                        if (pc.validarProducto(productos.getNombre()) != null) {
                            sesiones.setAttribute("mensajeError", "existe");
                            System.out.println("Ya existe");
                        } else {
                            sesiones.setAttribute("mensajeOk", "noExiste");
                            pc.create(productos);
                        }

                        response.sendRedirect(AppHelper.baseUrl() + "adminProductos");
                        System.out.println("Exito");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "/editProd":
                    try {
                        //actualizar archivos
                        boolean banderaUpd = false;
                        Productos productos = new Productos();
                        List<FileItem> multiparts;
                        multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (item.isFormField()) {
                                if (item.getFieldName().equals("idEdit")) {
                                    productos.setIdProd(Integer.parseInt(item.getString()));
                                }
                                if (item.getFieldName().equals("nombreEdit")) {
                                    productos.setNombre(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("desEdit")) {
                                    productos.setDescripcion(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("eleccionEdit")) {
                                    productos.setIdCat(new Categoriaproducto());
                                    productos.getIdCat().setIdCat(Integer.parseInt(item.getString()));
                                } 
                            } else {
                                if (banderaUpd == false) {
                                    String rutaImg;
                                    String nombreImg = new File(item.getName()).getName();
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/imgServer/";
                                    request.setAttribute("imgEdit", nombreImg);
                                    if(nombreImg.length()>0){
                                        File nuevaImagen = new File(rutaImg + nombreImg);
                                        try{
                                            item.write(nuevaImagen);
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                        System.out.println("Imagen del if "+nombreImg);
                                        productos.setImagen(nombreImg);
                                    }else{
                                        nombreImg =  pc.noImg(productos.getIdProd()).getImagen();
                                        System.out.println("Imagen "+nombreImg);
                                        productos.setImagen(nombreImg);
                                    }
                                } else {
                                    String rutaDoc;
                                    String nombreDoc = new File(item.getName()).getName();
                                    rutaDoc = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/docServer/";
                                    request.setAttribute("docEdit", nombreDoc);
                                   if(nombreDoc.length()>0){
                                        File nuevoDoc = new File(rutaDoc + nombreDoc);
                                        try{
                                            item.write(nuevoDoc);
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                        productos.setDescDoc(nombreDoc);
                                    }else{
                                        nombreDoc =  pc.noImg(productos.getIdProd()).getDescDoc();
                                        System.out.println("Doc "+nombreDoc);
                                        productos.setDescDoc(nombreDoc);
                                    }
                                }
                                banderaUpd= true;
                            }
                            
                        }
                        productos.setStatus(1);
                        if ((pc.validarProducto(productos.getNombre()) == null) || (Objects.equals(pc.validarProducto(productos.getNombre()).getNombre(), productos.getNombre()))){
                            sesiones.setAttribute("mensajeOk", "noExiste");
                            pc.edit(productos);
                        } else {
                            sesiones.setAttribute("mensajeError", "existe");
                            System.out.println("Ya existe");
                        }
                        response.sendRedirect(AppHelper.baseUrl() + "adminProductos");
                        System.out.println("Exito");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "/delProd":
                    int id = Integer.parseInt(request.getParameter("producto"));
                    pc.eliminarP(id);
                    break;
                default:
                    request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                    break;
            }
        }
    }

}
