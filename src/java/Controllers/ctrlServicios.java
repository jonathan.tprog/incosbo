/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Entity.Categoriaservicio;
import Entity.Servicio;
import Facade.ServicioFacade;
import Helpers.AppHelper;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Negrito
 */
@WebServlet(name = "ctrlServicios", urlPatterns = {"/nuevoServicio", "/editServ", "/delServ"})
public class ctrlServicios extends HttpServlet {

    @EJB
    private ServicioFacade sf;

    HttpSession sesiones;
    boolean bandera = false;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sesiones = (HttpSession) request.getSession();
        if (AppHelper.tieneAcceso(this.sesiones, 1) || AppHelper.tieneAcceso(this.sesiones, 2)) {
            switch (request.getServletPath()) {
                case "/nuevoServicio":
                    System.out.println("En nuevo servicio");
                    try {
                        //Subir archivos
                        Servicio servicio = new Servicio();
                        List<FileItem> multiparts;
                        multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (item.isFormField()) {
                                if (item.getFieldName().equals("nombreModal")) {
                                    servicio.setNombre(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("desModal")) {
                                    servicio.setDescripcion(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("eleccion")) {
                                    servicio.setIdcatSer(new Categoriaservicio());
                                    servicio.getIdcatSer().setIdcatSer(Integer.parseInt(item.getString()));
                                }
                            } else {
                                String rutaImg;
                                String nombreImg = new File(item.getName()).getName();
                                if (bandera == false) {
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/imgServer/";
                                    request.setAttribute("imgModal", nombreImg);
                                } else {
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/docServer/";
                                    request.setAttribute("docModal", nombreImg);
                                }
                                File nuevaImagen = new File(rutaImg + nombreImg);
                                System.out.println("Imagen: " + nuevaImagen);
                                item.write(nuevaImagen);
                                if (bandera == false) {
                                    servicio.setImagen(nombreImg);
                                } else {
                                    servicio.setDescDoc(nombreImg);
                                }

                                bandera = true;
                            }
                        }
                        servicio.setStatus(1);
                        if (sf.validarServicio(servicio.getNombre()) != null) {
                            sesiones.setAttribute("mensajeError", "existe");
                            System.out.println("Ya existe");
                        } else {
                            sesiones.setAttribute("mensajeOk", "noExiste");
                            sf.create(servicio);
                        }
                        response.sendRedirect(AppHelper.baseUrl() + "adminServicios");
                        System.out.println("Exito");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "/editServ":
                    try {
                        //actualizar archivos
                        boolean banderaUpd = false;
                        Servicio servicio = new Servicio();
                        List<FileItem> multiparts;
                        multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (item.isFormField()) {
                                if (item.getFieldName().equals("idEdit")) {
                                    servicio.setIdSer(Integer.parseInt(item.getString()));
                                }
                                if (item.getFieldName().equals("nombreEdit")) {
                                    servicio.setNombre(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("desEdit")) {
                                    servicio.setDescripcion(new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
                                }
                                if (item.getFieldName().equals("eleccionEdit")) {
                                    servicio.setIdcatSer(new Categoriaservicio());
                                    servicio.getIdcatSer().setIdcatSer(Integer.parseInt(item.getString()));
                                }
                            } else {
                               if (banderaUpd == false) {
                                    String rutaImg;
                                    String nombreImg = new File(item.getName()).getName();
                                    rutaImg = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/imgServer/";
                                    request.setAttribute("imgEdit", nombreImg);
                                    if(nombreImg.length()>0){
                                        File nuevaImagen = new File(rutaImg + nombreImg);
                                        try{
                                            item.write(nuevaImagen);
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                        System.out.println("Imagen del if "+nombreImg);
                                        servicio.setImagen(nombreImg);
                                    }else{
                                        nombreImg =  sf.noImg(servicio.getIdSer()).getImagen();
                                        System.out.println("Imagen "+nombreImg);
                                        servicio.setImagen(nombreImg);
                                    }
                                } else {
                                    String rutaDoc;
                                    String nombreDoc = new File(item.getName()).getName();
                                    rutaDoc = "/opt/payara5/glassfish/domains/domain1/applications/incosbo/docServer/";
                                    request.setAttribute("docEdit", nombreDoc);
                                   if(nombreDoc.length()>0){
                                        File nuevoDoc = new File(rutaDoc + nombreDoc);
                                        try{
                                            item.write(nuevoDoc);
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                        servicio.setDescDoc(nombreDoc);
                                    }else{
                                        nombreDoc =  sf.noImg(servicio.getIdSer()).getDescDoc();
                                        System.out.println("Doc "+nombreDoc);
                                        servicio.setDescDoc(nombreDoc);
                                    }
                                }
                                banderaUpd= true;
                            }
                        }
                        servicio.setStatus(1);
                        if ((sf.validarServicio(servicio.getNombre()) == null) || (Objects.equals(sf.validarServicio(servicio.getNombre()).getNombre(), servicio.getNombre()))) {
                            sesiones.setAttribute("mensajeOk", "noExiste");
                            sf.edit(servicio);
                        } else {
                            sesiones.setAttribute("mensajeError", "existe");
                            System.out.println("Ya existe");
                        }
                        response.sendRedirect(AppHelper.baseUrl() + "adminServicios");
                        System.out.println("Exito");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "/delServ":
                    int id = Integer.parseInt(request.getParameter("servicio"));
                    sf.eliminarS(id);
                    break;
                default:
                    request.getRequestDispatcher("WEB-INF/views/principal.jsp").forward(request, response);
                    break;
            }
        }
    }//doPost
}