/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Negrito
 */
@Entity
@Table(name = "servicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s")
    , @NamedQuery(name = "Servicio.findByIdSer", query = "SELECT s FROM Servicio s WHERE s.idSer = :idSer")
    , @NamedQuery(name = "Servicio.findByNombre", query = "SELECT s FROM Servicio s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Servicio.findByDescripcion", query = "SELECT s FROM Servicio s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "Servicio.findByImagen", query = "SELECT s FROM Servicio s WHERE s.imagen = :imagen")
    , @NamedQuery(name = "Servicio.findByDescDoc", query = "SELECT s FROM Servicio s WHERE s.descDoc = :descDoc")
    , @NamedQuery(name = "Servicio.findByStatus", query = "SELECT s FROM Servicio s WHERE s.status = :status")})
public class Servicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ser")
    private Integer idSer;
    @Size(max = 200)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 3500)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 100)
    @Column(name = "imagen")
    private String imagen;
    @Size(max = 100)
    @Column(name = "descDoc")
    private String descDoc;
    @Column(name = "status")
    private Integer status;
    @JoinColumn(name = "id_catSer", referencedColumnName = "id_catSer")
    @ManyToOne
    private Categoriaservicio idcatSer;

    public Servicio() {
    }

    public Servicio(Integer idSer) {
        this.idSer = idSer;
    }

    public Integer getIdSer() {
        return idSer;
    }

    public void setIdSer(Integer idSer) {
        this.idSer = idSer;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescDoc() {
        return descDoc;
    }

    public void setDescDoc(String descDoc) {
        this.descDoc = descDoc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Categoriaservicio getIdcatSer() {
        return idcatSer;
    }

    public void setIdcatSer(Categoriaservicio idcatSer) {
        this.idcatSer = idcatSer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSer != null ? idSer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.idSer == null && other.idSer != null) || (this.idSer != null && !this.idSer.equals(other.idSer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Servicio[ idSer=" + idSer + " ]";
    }
    
}
