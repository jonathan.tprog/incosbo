/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Negrito
 */
@Entity
@Table(name = "categoriaproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriaproducto.findAll", query = "SELECT c FROM Categoriaproducto c")
    , @NamedQuery(name = "Categoriaproducto.findByIdCat", query = "SELECT c FROM Categoriaproducto c WHERE c.idCat = :idCat")
    , @NamedQuery(name = "Categoriaproducto.findByCategoria", query = "SELECT c FROM Categoriaproducto c WHERE c.categoria = :categoria")
    , @NamedQuery(name = "Categoriaproducto.findByStatus", query = "SELECT c FROM Categoriaproducto c WHERE c.status = :status")})
public class Categoriaproducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cat")
    private Integer idCat;
    @Size(max = 150)
    @Column(name = "categoria")
    private String categoria;
    @Column(name = "status")
    private Integer status;
    @OneToMany(mappedBy = "idCat")
    private List<Productos> productosList;

    public Categoriaproducto() {
    }

    public Categoriaproducto(Integer idCat) {
        this.idCat = idCat;
    }

    public Integer getIdCat() {
        return idCat;
    }

    public void setIdCat(Integer idCat) {
        this.idCat = idCat;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @XmlTransient
    public List<Productos> getProductosList() {
        return productosList;
    }

    public void setProductosList(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCat != null ? idCat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriaproducto)) {
            return false;
        }
        Categoriaproducto other = (Categoriaproducto) object;
        if ((this.idCat == null && other.idCat != null) || (this.idCat != null && !this.idCat.equals(other.idCat))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Categoriaproducto[ idCat=" + idCat + " ]";
    }
    
}
