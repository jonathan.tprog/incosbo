/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Negrito
 */
@Entity
@Table(name = "categoriaservicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriaservicio.findAll", query = "SELECT c FROM Categoriaservicio c")
    , @NamedQuery(name = "Categoriaservicio.findByIdcatSer", query = "SELECT c FROM Categoriaservicio c WHERE c.idcatSer = :idcatSer")
    , @NamedQuery(name = "Categoriaservicio.findByServicio", query = "SELECT c FROM Categoriaservicio c WHERE c.servicio = :servicio")})
public class Categoriaservicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_catSer")
    private Integer idcatSer;
    @Size(max = 150)
    @Column(name = "servicio")
    private String servicio;
    @OneToMany(mappedBy = "idcatSer")
    private List<Servicio> servicioList;

    public Categoriaservicio() {
    }

    public Categoriaservicio(Integer idcatSer) {
        this.idcatSer = idcatSer;
    }

    public Integer getIdcatSer() {
        return idcatSer;
    }

    public void setIdcatSer(Integer idcatSer) {
        this.idcatSer = idcatSer;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    @XmlTransient
    public List<Servicio> getServicioList() {
        return servicioList;
    }

    public void setServicioList(List<Servicio> servicioList) {
        this.servicioList = servicioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcatSer != null ? idcatSer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriaservicio)) {
            return false;
        }
        Categoriaservicio other = (Categoriaservicio) object;
        if ((this.idcatSer == null && other.idcatSer != null) || (this.idcatSer != null && !this.idcatSer.equals(other.idcatSer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Categoriaservicio[ idcatSer=" + idcatSer + " ]";
    }
    
}
