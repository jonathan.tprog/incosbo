/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import java.security.MessageDigest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Negrito
 */
public final class AppHelper {

    private AppHelper() {
    }

    static int valida = 0;

    public static int getValida() {
        return valida;
    }

    public static void setValida(int valida) {
        AppHelper.valida = valida;
    }

    public static String baseUrl() {
        //Local
        //return "http://localhost:8080/ProyectoIncosbo/";
        //Prod
        //return "www.incosbo.com/";
        return "http://138.197.204.239/";
    }
    


    public static String defaultController() {
        return "inicio";

    }

    public static String convertirMD5(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(text.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean tieneAcceso(HttpSession sesiones, int TipoUsu) {
        if (sesiones != null) {
            if (sesiones.getAttribute("tpUsu") != null) {
                if (sesiones.getAttribute("tpUsu").equals(TipoUsu)) {
                    return true;
                }
            }
        }
        return false;
    }
}